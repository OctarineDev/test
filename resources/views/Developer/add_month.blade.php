@extends('layouts.header')

@section('content')
        <p>Информация о сотруднике</p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <a href="/update_developer_{{$developer->id}}">Редактировать</a>

    <p><label>Ф.И.О.</label> {{ $developer->name }}</p>
    <p><label>Должность</label> {{ $developer->work_type }}</p>
    <p><label>Формат работы</label> {{ $developer->type_salary }}</p>
    <p><label>З\п</label> {{ $developer->salary }}</p>
    <p><label>Номер телефона</label> {{ $developer->phone }}</p>
    <p><label>skype</label> {{ $developer->skype }}</p>
    <p><label>E-mail</label> {{ $developer->mail }}</p>
    <p><label>Адресс</label> {{ $developer->adress }}</p>
    <p><label>Номер карты</label> {{ $developer->card }}</p>

        <form action="{{ route('add.month') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $developer->id }}" >
          <p><label>Месяц и год</label>
              <select name="month" ><option value="">---</option>
                      @foreach ($months as $month)
                  <option value="{{ $month }}">{{ $month }}</option>
                  @endforeach
              </select>
              <select name="year" ><option value="">---</option>
                  @foreach ($years as $year)
                  <option value="{{ $year }}">{{ $year }}</option>
                  @endforeach
              </select>
          </p>

            <input type="submit" value="Добавить месяц">

        </form>
@endsection