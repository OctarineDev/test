@extends('layouts.header')

@section('content')
<p>{{ $stageproject->project_type }}
    <a href="/Project_{{ $id }}/update_stageproject_{{$stageproject->id}}">Редактировать</a>
    <a href="/Project_{{ $id }}/stageproject_{{$stageproject->id}}/addStageDev">Добавить работу</a></p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{ route('update.stagedev') }}" method="POST">
        {{ csrf_field() }}
        <input name="id" value="{{ $stagedev->id }}" type="hidden">
        <input name="id_project" value="{{ $stagedev->id_project }}" type="hidden">
        <input name="id_stage" value="{{ $stagedev->id_stage }}" type="hidden">
        <p><label>Вид работы</label><input type="" name="work_type" value="{{ $stagedev->work_type }}" ></p>
        <p><label>Исполнитель</label>
        <select name="id_dev">
            @foreach ($developers as $developer)
            @if ($stagedev->name_dev == $developer->name)
            <option selected value="{{ $developer->id }}">{{ $developer->name }}</option>
            @else
            <option value="{{ $developer->id }}">{{ $developer->name }}</option>
            @endif
            @endforeach
        </select>
        </p>
        <p><label>Кол-во часов </label><input type="" name="hour" value="{{ $stagedev->hour }}" ></p>
        <p><label>Час (USD) </label><input type="" name="hour_price" value="{{ $stagedev->hour_price }}" ></p>
        <p><label>Общий долг (USD) </label><input type="" name="total_price" value="{{ $stagedev->total_price }}" ></p>
        <p><label>Дата начала</label>
            <select name="day_start" ><option value="">---</option>
                @foreach ($days as $day)
                @if($day == $stagedev->day_start)
                <option selected value="{{ $day }}">{{ $day }}</option>
                @else
                <option  value="{{ $day }}">{{ $day }}</option>
                @endif
                @endforeach
            </select>
            <select name="month_start" ><option value="">---</option>
                @foreach ($months as $month)
                @if($month == $stagedev->month_start)
                <option selected value="{{ $month }}">{{ $month }}</option>
                @else
                <option value="{{ $month }}">{{ $month }}</option>
                @endif
                @endforeach
            </select>
            <select name="year_start" ><option value="">---</option>
                @foreach ($years as $year)
                @if($year == $stagedev->year_start)
                <option selected value="{{ $year }}">{{ $year }}</option>
                @else
                <option value="{{ $year }}">{{ $year }}</option>
                @endif
                @endforeach
            </select>
        </p>
        <p><label>Дата окончания</label>

            <select name="day_finish" ><option value="">---</option>
                @foreach ($days as $day)
                @if($day == $stagedev->day_finish)
                <option selected value="{{ $day }}">{{ $day }}</option>
                @else
                <option  value="{{ $day }}">{{ $day }}</option>
                @endif
                @endforeach
            </select>
            <select name="month_finish" ><option value="">---</option
                @foreach ($months as $month)
                @if($month == $stagedev->month_finish)
                <option selected value="{{ $month }}">{{ $month }}</option>
                @else
                <option value="{{ $month }}">{{ $month }}</option>
                @endif
                @endforeach
            </select>
            <select name="year_finish" ><option value="">---</option>
                @foreach ($years as $year)
                @if($year == $stagedev->year_finish)
                <option selected value="{{ $year }}">{{ $year }}</option>
                @else
                <option value="{{ $year }}">{{ $year }}</option>
                @endif
                @endforeach
            </select>
        </p>
        <p><label>Комментарий</label><textarea name="comments" >{{ $stagedev->comments }}</textarea></p>

        <input type="submit" value="Обновить">

    </form>


@endsection