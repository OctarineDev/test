@extends('layouts.header')

@section('content')
<p>Информация о всём проекте</p>
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<table>

    <tr>
        <td>Навзание проекта</td>
        <td>Заказчик</td>
        <td>КОмпания</td>
        <td>Телефон</td>
        <td>Скайп</td>
        <td>Почта</td>
    </tr>
    <a href="/update_project_{{$project->id}}">Редактировать</a>
    <tr>
        <td>{{ $project->project_name }}</td>
        <td>{{ $project->client_name }}</td>
        <td>{{ $project->company_name }}</td>
        <td>{{ $project->phone }}</td>
        <td>{{ $project->skype }}</td>
        <td>{{ $project->mail }}</td>
    </tr>

</table>
<p>Добавление нового под-проекта </p>
<p>{{ $calculation->comments }}
    <a href="/Project_{{ $calculation->id_project }}/update_calculation_{{ $calculation->id }}">Редактировать</a></p>

<tr>
    <p><label>Дата</label>{{ $calculation->day_payment }} {{ $calculation->month_payment }} {{ $calculation->year_payment }}</p>
    <p><label>Сумма в USD</label>{{ $calculation->total_price }}</p>
</tr>

@endsection

