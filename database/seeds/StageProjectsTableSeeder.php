<?php

use Illuminate\Database\Seeder;
use App\StageProject as StageProject;


class StageProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StageProject::truncate();

        DB::table('stageprojects')->insert([
            'id_project' => '1',
            'project_type' => 'Разработка Сайта',
            'status' => 'Открыт',


        ]);
        DB::table('stageprojects')->insert([
            'id_project' => '2',
            'project_type' => 'Реклама',
            'status' => 'Открыт',

        ]);
    }
}
