<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StageProject extends Model
{
    protected $table = "stageprojects";

    public function project(){

        return $this->belongsTo('App\Project','id_project');
    }

    public function stagedevs(){

        return $this->hasMany('App\StageDev','id_stage');
    }
}
