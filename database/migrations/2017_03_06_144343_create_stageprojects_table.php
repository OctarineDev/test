<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageprojectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stageprojects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_project');
            $table->string('project_type'); ;
            $table->string('id_dev'); ;
            $table->string('name_dev'); ;
            $table->string('project_percent'); ;
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stageprojects');
    }
}
