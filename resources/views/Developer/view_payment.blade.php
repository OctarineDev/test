@extends('layouts.header')

@section('content')
<p>Информация о сотруднике</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


<a href="/update_developer_{{$developers->id}}">Редактировать</a>

<p><label>Ф.И.О.</label> {{ $developers->name }}</p>
<p><label>Должность</label> {{ $developers->work_type }}</p>
<p><label>Формат работы</label> {{ $developers->type_salary }}</p>
<p><label>З\п</label> {{ $developers->salary }}</p>
<p><label>Номер телефона</label> {{ $developers->phone }}</p>
<p><label>skype</label> {{ $developers->skype }}</p>
<p><label>E-mail</label> {{ $developers->mail }}</p>
<p><label>Адресс</label> {{ $developers->adress }}</p>
<p><label>Номер карты</label> {{ $developers->card }}</p>



   <p> <h1> {{ $payments->name_project }}</h1><a href="/Developer_{{ $payments->id_developer }}/month_{{ $payments->id_month }}/update_payment_{{$payments->id}}">Редактировать</a></p>
    <p>Месяц: {{ $payments->month['month'] }} {{ $payments->month['year'] }}</p>
    <p>Оплачено часов: {{ $payments->payment_hour }}</p>
    <p>Оплачено: {{ $payments->payment_project }} USD</p>
    <p>Комментарий: {{ $payments->comments }}</p>






@endsection