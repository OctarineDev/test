@extends('layouts.header')

@section('content')
        <p>Информация о всём проекте</p>

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="{{ route('add.project') }}" id="check-form" method="POST">
            {{ csrf_field() }}

          <p><label>Название проекта</label><input type="" name="project_name" value="{{ old('project_name') }}" >
            <label>Имя заказчика</label><input type="" name="client_name" value="{{ old('client_name') }}" >
            <label>Номер телефона</label><input type="" name="phone" value="{{ old('phone') }}" >
            <label>Название компании</label><input type="" name="company_name" value="{{ old('company_name') }}"></p>
            <p><label>Статус</label>  <select name="status">
                    <option value="Открыт">Открыт</option>
                    <option value="Закрыт">Закрыт</option>
                </select>
            <label>Скайп</label><input type="" name="skype" value="{{ old('skype') }}">
            <label>Почта</label><input type="" name="mail" value="{{ old('mail') }}"></p>

            <input type="submit"  value="Добавить проект">


        </form>


@endsection