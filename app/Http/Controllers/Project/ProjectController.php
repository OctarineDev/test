<?php
namespace App\Http\Controllers\Project;
header('Content-type: application/json');


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Response;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Project;
use App\StageProject;
use App\StageDev;
use App\Calculation;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class ProjectController extends Controller
{

    use ValidatesRequests;
    
    public function index(){

    $projects = Project::all();
     foreach($projects as $project){
     $calculations = Calculation::where('id_project', $project->id)->get();
      $stages = Project::find($project->id)->stageprojects()->get();
      $total = array();

      $total_debt = 0;
        foreach($stages as $stage){

            $i=0;
            $stageprojects = StageProject::find($stage->id)->stagedevs()->get();
            $total_price = 0;
            foreach($stageprojects as $stageproject){
                $total_price+= $stageproject->total_price;
            }
            $stage['total_price'] = $total_price;
            $total_debt+=$total_price;

        }

        $total_sum = 0;
        foreach($calculations as $calculation){
            $total_sum+= $calculation->total_price;

        }
         $sum = $total_debt - $total_sum;
         $project['sum'] = $sum;
     }

    return view('project.all_projects')->with(compact(['projects']));

    }

    public function deleteProject($id)
    {

        Project::find($id)->delete();

        $projects = Project::all();
        return view('project.all_projects')->with(compact(['projects']));

    }

    public function viewProject($id){
        $project = Project::find($id);
        $calculations = Calculation::where('id_project', $id)->get();
        $stages = Project::find($id)->stageprojects()->get();
        $total = array();

        $total_debt = 0;
        foreach($stages as $stage){

            $i=0;
        $stageprojects = StageProject::find($stage->id)->stagedevs()->get();
            $total_price = 0;
            foreach($stageprojects as $stageproject){
                $total_price+= $stageproject->total_price;
            }
            $stage['total_price'] = $total_price;
               $total_debt+=$total_price;

        }

        $total_sum = 0;
        foreach($calculations as $calculation){
            $total_sum+= $calculation->total_price;

        }

        return view('project.view_project')->with(compact(['project'],['stages'],['total_prices'],['calculations'],['id'],['total_sum'],['total_debt']));

    }
    public function viewForm(){

        return view('project.add_project');

    }
    public function updateForm($id){

        $projects = Project::find($id);
        return view('project.update_project')->with(compact(['projects']));
    }

    public function addProject(){

        Input::flash();

        $rules = [
            'project_name' => 'required|unique:projects|max:255',
            'client_name' => 'required',
            'status' => 'required',
            'phone' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $project = new Project();

        $project_name = Input::get('project_name');
        $client_name = Input::get('client_name');
        $company_name = Input::get('company_name');
        $status = Input::get('status');
        $phone = Input::get('phone');
        $skype = Input::get('skype');
        $mail = Input::get('mail');

        $project->project_name = $project_name;
        $project->client_name = $client_name;
        $project->company_name = $company_name;
        $project->status = $status;
        $project->phone = $phone;
        $project->skype = $skype;
        $project->mail = $mail;

        $project->save();

        $projects = Project::all();
        return view('project.all_projects')->with(compact(['projects']));

    }
    public function updateProject(){
        $rules = [
            'project_name' => 'required|max:255',
            'client_name' => 'required',
            'status' => 'required',
            'phone' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $id=Input::get('id');

         $project = Project::where('id', $id)->first();

        $project_name = Input::get('project_name');
        $client_name = Input::get('client_name');
        $company_name = Input::get('company_name');
        $status = Input::get('status');
        $phone = Input::get('phone');
        $skype = Input::get('skype');
        $mail = Input::get('mail');

        $project->project_name = $project_name;
        $project->client_name = $client_name;
        $project->company_name = $company_name;
        $project->status = $status;
        $project->phone = $phone;
        $project->skype = $skype;
        $project->mail = $mail;

        $project->save();

        return redirect('/Project_'.$id);

    }

}
