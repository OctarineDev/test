<?php

namespace App\Http\Controllers\Developer;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\Project;
use App\Month;
use App\StageDev;
use App\Developer;
use App\Payment;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class PaymentController extends Controller
{


    public function viewForm($id_developer,$id_month){

        $developer = Developer::find($id_developer);
        $projects = Project::where('status', 'Открыт')->get();
        foreach($projects as $project){
        $stagedev = StageDev::where('id_dev', $id_developer)
                                ->where('id_project', $project->id)
                                ->first();
            if($stagedev !== NULL){

                $developer_project[] = $project;// проекты в которых задействован разработчик
            }
        }
        unset($projects);
        $projects = $developer_project;

        return view('developer.add_month_payment')->with(compact(['developer'],['id_month'],['projects']));

    }

    public function viewPayment($id,$id_month,$id_payment){

        $developers = Developer::find($id);
        $payments = Payment::find($id_payment);

        return view('developer.view_payment')->with(compact(['developers'],['payments']));

    }

    public function updateForm($id_developer,$id_month,$id_payment){

        $developer = Developer::find($id_developer);
        $payment = Payment::find($id_payment);
        $projects = Project::where('status', 'Открыт')->get();
        foreach($projects as $project){
            $stagedev = StageDev::where('id_dev', $id_developer)
                ->where('id_project', $project->id)
                ->first();
            if($stagedev !== NULL){

                $developer_project[] = $project;// проекты в которых задействован разработчик
            }
        }
        unset($projects);
        $projects = $developer_project;


        return view('developer.update_payment')->with(compact(['developer'],['payment'],['projects']));
    }

    public function addMonthPayment(){

        Input::flash();

        $rules = [
            'name_project' => 'required|max:255',
            'payment_hour' => 'required|max:255',
            'payment_project' => 'required',
            'comments' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $payment = new Payment();

        $id_developer = Input::get('id_developer');
        $id_month = Input::get('id_month');
        $project_name = Input::get('name_project');
        $payment_hour = Input::get('payment_hour');
        $payment_project = Input::get('payment_project');
        $comments = Input::get('comments');

        $project = Project::where('project_name', $project_name)->first();
        $id_project = $project->id;

        $payment->id_developer = $id_developer;
        $payment->id_month = $id_month;
        $payment->id_project = $id_project;
        $payment->name_project = $project_name;
        $payment->payment_hour = $payment_hour;
        $payment->payment_project = $payment_project;
        $payment->comments = $comments;

        $payment->save();

        return redirect('/Developer_'.$id_developer.'/month_'.$id_month);

    }
    public function updateMonthPayment(){
        $rules = [
            'name_project' => 'required|max:255',
            'payment_hour' => 'required|max:255',
            'payment_project' => 'required',
            'comments' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $id= Input::get('id');
        $id_developer = Input::get('id_developer');
        $id_month = Input::get('id_month');
        $project_name = Input::get('name_project');
        $payment_hour = Input::get('payment_hour');
        $payment_project = Input::get('payment_project');
        $comments = Input::get('comments');

        $project = Project::where('project_name', $project_name)->first();
        $id_project = $project->id;

        $payment = Payment::where('id', $id)->first();

        $payment->id_project = $id_project;
        $payment->name_project = $project_name;
        $payment->payment_hour = $payment_hour;
        $payment->payment_project = $payment_project;
        $payment->comments = $comments;

        $payment->save();

        return redirect('/Developer_'.$id_developer.'/month_'.$id_month.'/payment_'.$id);


    }


}
