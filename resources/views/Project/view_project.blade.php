@extends('layouts.header')

@section('content')
<p>Информация о всём проекте</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
<a href="/update_project_{{$project->id}}">Редактировать</a>
<!--<form action="/deleteProject_{{$project->id}}" method="POST">-->
<!--    {{ csrf_field() }}-->
<!--    <input type="submit" value="Удалить" />-->
<!--</form>-->
    <table>

        <tr>
            <td>Навзание проекта</td>
            <td>Заказчик</td>
            <td>КОмпания</td>
            <td>Телефон</td>
            <td>Скайп</td>
            <td>Почта</td>
        </tr>

        <tr>
            <td>{{ $project->project_name }}</td>
            <td>{{ $project->client_name }}</td>
            <td>{{ $project->company_name }}</td>
            <td>{{ $project->phone }}</td>
            <td>{{ $project->skype }}</td>
            <td>{{ $project->mail }}</td>
        </tr>


    </table>

    <p>Работы  <a href="/addStageProject/project_id={{$project->id}}">Добавить работу</a></p>
    <table>
        <tr>
            <td>Работа</td>
            <td>Статус</td>
            <td>Сумма за работу</td>

        </tr>
        @foreach ($stages as $stage)
        <tr onclick="window.location.href='/Project_{{$stage->id_project}}/stageproject_{{$stage->id}}';">
            <td>{{ $stage->project_type }}</td>
            <td>{{ $stage->status }}</td>
            <td>{{ $stage['total_price'] }} USD</td>
        </tr>
        @endforeach
    </table>
<p><span>Расчеты</span><a href="/Project_{{ $id }}/addCalculation">Добавить расчет</a></p>
<table>
    <tr>
        <td>Комментарий</td>
        <td>Дата</td>
        <td>Сумма</td>

    </tr>
    @foreach ($calculations as $calculation)
    <tr onclick="window.location.href='/Project_{{$calculation->id_project}}/calculation_{{$calculation->id}}';">
        <td>{{ $calculation->comments }}</td>
        <td>{{ $calculation->day_payment }} {{ $calculation->month_payment }} {{ $calculation->year_payment }} </td>
        <td>{{ $calculation->total_price }} USD</td>
    </tr>
    @endforeach


</table>
<p>Уплачено: {{ $total_sum }}</p>
<p>Общий долг: {{ $total_debt }}</p>
<p>К выплате: {{ $total_debt - $total_sum}}</p>
@endsection