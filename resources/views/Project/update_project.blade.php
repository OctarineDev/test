@extends('layouts.header')

@section('content')
        <p>Информация о всём проекте</p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ route('update.project')}}" method="POST">
            {{ csrf_field() }}

            <input type="hidden" name="id" value="{{ $projects->id }}">
            <p><label>Название проекта</label><input type="" name="project_name" value="{{ $projects->project_name }}">
            <label>Имя заказчика</label><input type="" name="client_name" value="{{ $projects->client_name }}">
            <label>Название компании</label><input type="" name="company_name" value="{{ $projects->company_name }}">
            <label>Номер телефона</label><input type="" name="phone" value="{{ $projects->phone }}"></p>
            <p><label>Статус</label> <select name="status">
                    @if ($projects->status == 'Открыт')
                    <option selected value="Открыт">Открыт</option>
                    <option  value="Закрыт">Закрыт</option>
                    @else if($projects->status == 'Закрыт')
                    <option  value="Открыт">Открыт</option>
                    <option selected value="Закрыт">Закрыт</option>
                    @endif
                </select>
            <label>Скайп</label><input type="" name="skype" value="{{ $projects->skype }}">
            <label>Почта</label><input type="" name="mail" value="{{ $projects->mail }}"></p>

            <input type="submit" value="Обновить проект">

        </form>
@endsection