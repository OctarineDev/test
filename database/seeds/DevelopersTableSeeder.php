<?php

use Illuminate\Database\Seeder;
use App\Developer as Developer;


class DevelopersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Developer::truncate();

        DB::table('developers')->insert([
            'name' => 'Сахно Анатолий',
            'work_type' => 'backend',
            'type_salary' => 'По проектно',
            'salary' => '5 usd/час',
            'phone' => '+38 (093) 961-42-254',
            'adress' => 'Луна, 2 кратор , мусорка справа',
            'card' => '52646474963246789',
            'skype' => '',
            'mail' => '1sakhno.tolik@gmail.com',

        ]);
        DB::table('developers')->insert([
            'name' => 'Романова Анна',
            'work_type' => 'frontend',
            'type_salary' => 'По проектно',
            'salary' => '2 usd/час',
            'phone' => '+38 (066) 66-66-666',
            'adress' => 'Луна, 2 кратор , мусорка справа',
            'card' => '52646474963246789',
            'skype' => '',
            'mail' => '',

        ]);
    }
}
