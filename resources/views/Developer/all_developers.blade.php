@extends('layouts.header')

@section('content')
    <p>Информация о сотрудниках</p>
<a href="/addDeveloper">Добавить сотрудника</a>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <table>
        <tr>
            <td>Ф.И.О.</td>
            <td>Должность</td>
            <td>З\п</td>
            <td>Долг</td>
        </tr>
        @foreach ($developers as $developer)
        <tr onclick="window.location.href='/Developer_{{$developer->id}}';">

            <td>{{ $developer->name }}</td>
            <td>{{ $developer->work_type }}</td>
            <td>{{ $developer->salary }} $\час</td>
            <td>{{ $developer->total_debt }} USD</td>
        </tr>

        @endforeach
    </table>
@endsection