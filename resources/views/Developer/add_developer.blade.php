@extends('layouts.header')

@section('content')
        <p>Информация о сотруднике</p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ route('add.developer') }}" method="POST">
            {{ csrf_field() }}

          <p><label>Ф.И.О.</label><input type="" name="name" value="{{ old('work_type') }}" ></p>
          <p><label>Должность</label><input type="" name="work_type" value="{{ old('work_type') }}" ></p>
          <p><label>Формат работы</label><select size="1" name="type_salary" >
                  <option value="1">По ЗП</option>
                  <option value="2">По проектно</option>
              </select></p>
          <p><label>З\п</label><input type="" name="salary" value="{{ old('salary') }}" ></p>
          <p><label>Номер телефона</label><input type="" name="phone" value="{{ old('phone') }}" ></p>
          <p><label>skype</label><input type="" name="skype" value="{{ old('skype') }}" ></p>
          <p><label>E-mail</label><input type="" name="mail" value="{{ old('mail') }}" ></p>
          <p><label>Адресс</label><input type="" name="adress" value="{{ old('adress') }}" ></p>
          <p><label>Номер карты</label><input type="" name="card" value="{{ old('card') }}" ></p>


            <input type="submit" value="Добавить сотрудника">

        </form>
@endsection