@extends('layouts.header')

@section('content')
    <p>Информация о всём проекте</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <table>

        <tr>
            <td>Навзание проекта</td>
            <td>Заказчик</td>
            <td>КОмпания</td>
            <td>Телефон</td>
            <td>Скайп</td>
            <td>Почта</td>
        </tr>
        <a href="/update_project_{{$project->id}}">Редактировать</a>
        <tr>
            <td>{{ $project->project_name }}</td>
            <td>{{ $project->client_name }}</td>
            <td>{{ $project->company_name }}</td>
            <td>{{ $project->phone }}</td>
            <td>{{ $project->skype }}</td>
            <td>{{ $project->mail }}</td>
        </tr>

    </table>
    <p>Информация о работе</p>

    <form action="{{ route('update.stageproject') }}" method="POST">
        {{ csrf_field() }}
        <input name="id_project" value="{{ $project->id }}" type="hidden">
        <input name="id" value="{{ $stageproject->id }}" type="hidden">
        <p><label>Наименование</label><input type="" name="project_type" value="{{ $stageproject->project_type }}" ></p>
        <p><label>Статус</label><select name="status">
                @if ($stageproject->status == 'Открыт')
                <option selected value="Открыт">Открыт</option>
                <option  value="Закрыт">Закрыт</option>
                @else if($stageproject->status == 'Закрыт')
                <option  value="Открыт">Открыт</option>
                <option selected value="Закрыт">Закрыт</option>
                @endif
            </select></p>
        <p><label>Кто привел заказчика</label><select name="id_dev">
            @foreach ($developers as $developer)
            @if ($stageproject->name_dev == $developer->name)
            <option selected value="{{ $developer->id }}">{{ $developer->name }}</option>
            @else
            <option value="{{ $developer->id }}">{{ $developer->name }}</option>
            @endif
            @endforeach
        </select>
        </p>
        <p>
            <label>Сумма за проект</label><input value="{{ $stageproject->project_percent }}"  name="project_percent" >
        </p>
        <input type="submit" value="Обновить">

    </form>

@endsection