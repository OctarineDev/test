<?php

namespace App\Http\Controllers\StageProject;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Project;
use App\StageProject;
use App\Developer;
use App\Month;
use App\Payment;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class StageProjectController extends Controller
{

    public function viewForm($id){

        $project = Project::find($id);
        $developers = Developer::all();
        return view('stageproject.add_stage_project')->with(compact(['project'],['developers']));


    }
    public function updateForm($id,$stage_id){

        $stageproject = StageProject::find($stage_id);
        $project = Project::find($id);
        $developers = Developer::all();


        return view('stageproject.update_stage_project')->with(compact(['project'],['stageproject'],['developers']));


    }

    public function addStageProject(){

        Input::flash();

        $rules = [
            'project_type' => 'required|max:255',
            'status' => 'required',
            'id_dev' => 'required',
            'project_percent' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }


        $stageproject = new StageProject();

        $project_type = Input::get('project_type');
        $status = Input::get('status');
        $id_project = Input::get('id_project');
        $id_dev = Input::get('id_dev');
        $project_percent = Input::get('project_percent');
        $developer = Developer::find($id_dev);

        $stageproject->project_type = $project_type;
        $stageproject->status = $status;
        $stageproject->id_project = $id_project;
        $stageproject->id_dev = $developer->id;
        $stageproject->name_dev = $developer->name;
        $stageproject->project_percent = $project_percent;

        $stageproject->save();

        $projects = Project::all();
        return redirect('/Project_'.$id_project);

    }

    public function updateStageProject(){
        $month_ru = array(
            1 => 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Авугст', 'Сентябрь', 'Октябрь', 'Ноябрь'
        );
        $cur_month =  $month_ru[date('n')];
        $cur_year = date('Y');

        $rules = [
            'project_type' => 'required|max:255',
            'status' => 'required',
            'id_dev' => 'required',
            'project_percent' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }


        $id_project = Input::get('id_project');
        $stage_id = Input::get('id');

        $stageproject = StageProject::
            where('id', $stage_id)
            ->where('id_project',$id_project)
            ->first();

        $project_type = Input::get('project_type');
        $status = Input::get('status');
        $id_project = Input::get('id_project');
        $id_dev = Input::get('id_dev');
        $project_percent = Input::get('project_percent');
        $developer = Developer::find($id_dev);

        $stageproject->project_type = $project_type;
        $stageproject->status = $status;
        $stageproject->id_project = $id_project;
        $stageproject->id_dev = $developer->id;
        $stageproject->name_dev = $developer->name;
        $stageproject->project_percent = $project_percent;

        if($status == 'Закрыт'){

        $month = Month::where('month',$cur_month)
                        ->where('year',$cur_year)
                        ->first();
        $project = Project::find($id_project)->first();

        $new_payment = new Payment();

        $new_payment->id_month = $month->id;
        $new_payment->id_project = $id_project;
        $new_payment->id_developer = $id_dev;
        $new_payment->name_project = $project->project_name;
        $new_payment->payment_hour = 0;
        $new_payment->payment_project = $project_percent;
        $new_payment->comments = 'Процент за сайт';

            $new_payment->save();
        }

        $stageproject->save();

        return redirect('/Project_'.$id_project.'/stageproject_'.$stage_id);

    }


}
