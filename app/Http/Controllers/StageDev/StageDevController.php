<?php

namespace App\Http\Controllers\StageDev;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Project;
use App\StageProject;
use App\StageDev;
use App\Developer;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class StageDevController extends Controller
{

    public function __construct(){
        $this->days = array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
        $this->months = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
        $this->years = array('2016','2017','2018','2019','2020');
    }

    public function viewStageDev($id,$stage_id){
        $project = Project::find($id);
        $stageproject = StageProject::find($stage_id);
        $stagedevs = StageDev::
            where('id_project', $id)
            ->where('id_stage',$stage_id)
            ->get();

        return view('stagedev.all_stage_devs')->with(compact(['project'],['stagedevs'],['stageproject']));


    }

    public function viewForm($id,$stage_id){

        $stageproject = StageProject::find($stage_id);
        $developers = Developer::all();
        $days =  $this->days;
        $months =  $this->months;
        $years =  $this->years;


        return view('stagedev.add_stage_dev')->with(compact(['stageproject'],['id'],['stage_id'],['developers'],['days'],['months'],['years']));

    }

    public function updateForm($id,$stage_id,$stagedev_id){

        $stageproject = StageProject::find($stage_id);
        $stagedev = StageDev::find($stagedev_id);
        $developers = Developer::all();
        $days =  $this->days;
        $months =  $this->months;
        $years =  $this->years;

        return view('stagedev.update_stage_dev')->with(compact(['stageproject'],['stagedev'],['id'],['stage_id'],['developers'],['days'],['months'],['years']));

    }

    public function addStageDev(){

        Input::flash();

        $rules = [
            'work_type' => 'required||max:255',
            'id_dev' => 'required',
            'day_start' => 'required',
            'month_start' => 'required',
            'year_start' => 'required',
            'day_finish' => 'required',
            'month_finish' => 'required',
            'year_finish' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }


        $stagedev = new StageDev();

        $id_project = Input::get('id_project');
        $id_stage = Input::get('id_stage');
        $work_type = Input::get('work_type');
        $id_dev = Input::get('id_dev');
        $developer = Developer::where('id',$id_dev)->first();
        $name_dev = $developer->name;
        $hour = Input::get('hour');
        $hour_price = Input::get('hour_price');
        $total_price = Input::get('total_price');
        $day_start = Input::get('day_start');
        $month_start = Input::get('month_start');
        $year_start = Input::get('year_start');
        $day_finish = Input::get('day_finish');
        $month_finish = Input::get('month_finish');
        $year_finish = Input::get('year_finish');
        $comments = Input::get('comments');



        $stagedev->id_project=$id_project;
        $stagedev->id_stage=$id_stage;
        $stagedev->work_type=$work_type;
        $stagedev->id_dev=$id_dev;
        $stagedev->name_dev=$name_dev;
        $stagedev->hour=$hour;
        $stagedev->hour_price=$hour_price;
        $stagedev->total_price=$total_price;
        $stagedev->day_start=$day_start;
        $stagedev->month_start=$month_start;
        $stagedev->year_start=$year_start;
        $stagedev->day_finish=$day_finish;
        $stagedev->month_finish=$month_finish;
        $stagedev->year_finish=$year_finish;
        $stagedev->comments=$comments;

        $stagedev->save();

        return redirect('/Project_'.$id_project.'/stageproject_'.$id_stage);

    }

    public function updateStageDev(){


        $rules = [
            'work_type' => 'required||max:255',
            'id_dev' => 'required',
            'day_start' => 'required',
            'month_start' => 'required',
            'year_start' => 'required',
            'day_finish' => 'required',
            'month_finish' => 'required',
            'year_finish' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $id = Input::get('id');
        $id_project = Input::get('id_project');
        $id_stage = Input::get('id_stage');

        $stagedev = StageDev::
            where('id', $id)
            ->where('id_project',$id_project)
            ->where('id_stage',$id_stage)
            ->first();

;
        $work_type = Input::get('work_type');
        $id_dev = Input::get('id_dev');
        $developer = Developer::where('id',$id_dev)->first();
        $name_dev = $developer->name;
        $hour = Input::get('hour');
        $hour_price = Input::get('hour_price');
        $total_price = Input::get('total_price');
        $day_start = Input::get('day_start');
        $month_start = Input::get('month_start');
        $year_start = Input::get('year_start');
        $day_finish = Input::get('day_finish');
        $month_finish = Input::get('month_finish');
        $year_finish = Input::get('year_finish');
        $comments = Input::get('comments');



        $stagedev->id_project=$id_project;
        $stagedev->id_stage=$id_stage;
        $stagedev->work_type=$work_type;
        $stagedev->id_dev=$id_dev;
        $stagedev->name_dev=$name_dev;
        $stagedev->hour=$hour;
        $stagedev->hour_price=$hour_price;
        $stagedev->total_price=$total_price;
        $stagedev->day_start=$day_start;
        $stagedev->month_start=$month_start;
        $stagedev->year_start=$year_start;
        $stagedev->day_finish=$day_finish;
        $stagedev->month_finish=$month_finish;
        $stagedev->year_finish=$year_finish;
        $stagedev->comments=$comments;

        $stagedev->save();

        return redirect('/Project_'.$id_project.'/stageproject_'.$id_stage);

    }





}
