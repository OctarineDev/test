<?php

use Illuminate\Database\Seeder;
use App\User as User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create( [
            'email' => 'rozhden.t@gmail.com' ,
            'password' => Hash::make( '41p1dros' ) ,
            'name' => 'Радик Падик' ,
        ] );
    }
}
