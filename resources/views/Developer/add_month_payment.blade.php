@extends('layouts.header')

@section('content')
        <p>Информация о сотруднике</p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <a href="/update_developer_{{$developer->id}}">Редактировать</a>

    <p><label>Ф.И.О.</label> {{ $developer->name }}</p>
    <p><label>Должность</label> {{ $developer->work_type }}</p>
    <p><label>Формат работы</label> {{ $developer->type_salary }}</p>
    <p><label>З\п</label> {{ $developer->salary }}</p>
    <p><label>Номер телефона</label> {{ $developer->phone }}</p>
    <p><label>skype</label> {{ $developer->skype }}</p>
    <p><label>E-mail</label> {{ $developer->mail }}</p>
    <p><label>Адресс</label> {{ $developer->adress }}</p>
    <p><label>Номер карты</label> {{ $developer->card }}</p>


        <form action="{{ route('add.month.payment') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="id_developer" value="{{ $developer->id }}" >
            <input type="hidden" name="id_month" value="{{ $id_month }}" >
          <p><label>Проект</label>
              <select name="name_project">
                  @foreach ($projects as $project)
                  <option value="{{ $project->project_name }}">{{ $project->project_name }}</option>
                  @endforeach
              </select>
          </p>
          <p><label>Оплачено часов</label><input type="" name="payment_hour" value="{{ old('payment_hour') }}" ></p>
          <p><label>Оплачено (USD)</label><input type="" name="payment_project" value="{{ old('payment_project') }}" ></p>
          <p><label>Комментарий</label><input type="" name="comments" value="{{ old('comments') }}" ></p>

            <input type="submit" value="Добавить отчет">

        </form>
@endsection