@extends('layouts.header')

@section('content')
<p>Статистика по месяцам</p>

    <table>

        <tr>
            <td>Месяц</td>
            <td>З.п</td>
            <td>Оплата</td>
            <td>Доход</td>
        </tr>
        @foreach ($months as $month)
        <tr>
            <td>{{ $month->month }} {{ $month->year }}</td>
            <td>{{ $month->total_debt }}</td>
            <td>{{ $month->total_salary }}</td>
            <td>{{ $month->total_debt-$month->total_salary }}</td>
        </tr>
        @endforeach
    </table>

<p>Статистика по проектам</p>


<table>
<h2>К выплате {{ $debt }}</h2>
    <tr>
        <td>Проект</td>
        <td>З.п</td>
        <td>Оплата</td>
        <td>Доход</td>
        <td>Долг</td>
    </tr>
    @foreach ($projects as $project)
    <tr>
        <td>{{ $project->project_name }}</td>
        <td>{{ $project->total_debt }}</td>
        <td>{{ $project->total_salary }}</td>
        <td>{{ $project->salary }}</td>
        <td>{{ $project->debt }}</td>
    </tr>
    @endforeach
</table>


@endsection