@extends('layouts.header')

@section('content')
<p>{{ $stageproject->project_type }}
    <a href="/Project_{{ $id }}/update_stageproject_{{$stageproject->id}}">Редактировать</a>
    <a href="/Project_{{ $id }}/stageproject_{{$stageproject->id}}/addStageDev">Добавить работу</a></p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


    <form action="{{ route('add.stagedev') }}" method="POST">
        {{ csrf_field() }}
        <input name="id_project" value="{{ $id }}" type="hidden">
        <input name="id_stage" value="{{ $stage_id }}" type="hidden">
        <p><label>Вид работы</label><input type="" name="work_type" value="{{ old('work_type') }}" ></p>
        <p><label>Исполнитель</label>
        <select name="id_dev">
            @foreach ($developers as $developer)
            <option value="{{ $developer->id }}">{{ $developer->name }}</option>
            @endforeach
        </select>
        </p>
        <p><label>Кол-во часов </label><input type="" name="hour" value="{{ old('hour') }}" ></p>
        <p><label>Час (USD) </label><input type="" name="hour_price" value="{{ old('hour_price') }}" ></p>
        <p><label>Общий долг (USD) </label><input type="" name="total_price" value="{{ old('total_price') }}" ></p>
        <p><label>Дата начала</label>
            <select name="day_start" ><option value="">---</option>
                @foreach ($days as $day)
                <option value="{{ $day }}">{{ $day }}</option>
                @endforeach
            </select>
            <select name="month_start" ><option value="">---</option>
                @foreach ($months as $month)
                <option value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
            <select name="year_start" ><option value="">---</option>
                @foreach ($years as $year)
                <option value="{{ $year }}">{{ $year }}</option>
                @endforeach
            </select>
        </p>
        <p><label>Дата окончания</label>

            <select name="day_finish" ><option value="">---</option>
                @foreach ($days as $day)
                <option value="{{ $day }}">{{ $day }}</option>
                @endforeach
            </select>
            <select name="month_finish" ><option value="">---</option
                    @foreach ($months as $month)
                <option value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
            <select name="year_finish" ><option value="">---</option>
                @foreach ($years as $year)
                <option value="{{ $year }}">{{ $year }}</option>
                @endforeach
            </select>
        </p>
        <p><label>Комментарий</label><textarea name="comments" value="{{ old('comments') }}" ></textarea></p>

        <input type="submit" value="Добавить">

    </form>
@endsection