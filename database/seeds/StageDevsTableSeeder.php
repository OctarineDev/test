<?php

use Illuminate\Database\Seeder;
use App\StageDev as StageDev;


class StageDevsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StageDev::truncate();

        DB::table('stagedevs')->insert([
            'id_project' => '1',
            'id_stage' => '1',
            'work_type' => 'Создание ЛК',
            'id_dev' => '4',
            'name_dev' => 'Сахно Анатолий',
            'hour' => '15',
            'hour_price' => '5',
            'total_price' => '75',
            'day_start' => '1',
            'month_start' => 'Январь',
            'year_start' => '2017',
            'day_finish' => '10',
            'month_finish' => 'Январь',
            'year_finish' => '2017',
            'comments' => 'ЛК:авторизация, валидация, формы',

        ]);
        DB::table('stagedevs')->insert([
            'id_project' => '2',
            'id_stage' => '1',
            'work_type' => 'Вёрстка ЛК',
            'id_dev' => '5',
            'name_dev' => 'Романова Анна',
            'hour' => '25',
            'hour_price' => '2',
            'total_price' => '50',
            'day_start' => '1',
            'month_start' => 'Январь',
            'year_start' => '2017',
            'day_finish' => '10',
            'month_finish' => 'Январь',
            'year_finish' => '2017',
            'comments' => 'Верстка:авторизации, формы',

        ]);
    }
}
