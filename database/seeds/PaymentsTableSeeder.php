<?php

use Illuminate\Database\Seeder;
use App\Payment as Payment;


class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Payment::truncate();

        DB::table('payments')->insert([
            'id_month' => '1',
            'id_project' => '1',
            'name_project' => 'Agro7trade',
            'id_developer' => '1',
            'comments' => 'Всё норм',
            'payment_hour' => '14',
            'payment_project' => '300',

        ]);
        DB::table('payments')->insert([
            'id_month' => '2',
            'id_project' => '2',
            'name_project' => 'Agro7trade',
            'id_developer' => '2',
            'comments' => 'нет слов',
            'payment_hour' => '50',
            'payment_project' => '100',
        ]);
    }




}
