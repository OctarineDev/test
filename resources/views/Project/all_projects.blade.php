@extends('layouts.header')

@section('content')
<p>Информация о всём проекте</p>
    <a href="/newProject">Добавить проект</a>

    <table>
        <tr>
            <td>Навзание проекта</td>
            <td>Заказчик</td>
            <td>Долг</td>
        </tr>
        @foreach ($projects as $project)
            <tr onclick="window.location.href='/Project_{{$project->id}}';">

                <td>{{ $project->project_name }}</td>
                <td>{{ $project->client_name }}</td>
                <td>{{ $project->sum }}</td>
            </tr>

        @endforeach
    </table>
@endsection