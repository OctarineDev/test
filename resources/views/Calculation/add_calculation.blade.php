@extends('layouts.header')

@section('content')
    <p>Информация о всём проекте</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <table>

        <tr>
            <td>Навзание проекта</td>
            <td>Заказчик</td>
            <td>КОмпания</td>
            <td>Телефон</td>
            <td>Скайп</td>
            <td>Почта</td>
        </tr>
        <a href="/update_project_{{$project->id}}">Редактировать</a>
        <tr>
            <td>{{ $project->project_name }}</td>
            <td>{{ $project->client_name }}</td>
            <td>{{ $project->company_name }}</td>
            <td>{{ $project->phone }}</td>
            <td>{{ $project->skype }}</td>
            <td>{{ $project->mail }}</td>
        </tr>

    </table>
    <p>Добавление нового под-проекта </p>
    <form action="{{ route('add.calculation') }}" method="POST">
        {{ csrf_field() }}
        <input name="id_project" value="{{ $project->id }}" type="hidden">
        <p><label>Коментарий</label><input type="" name="comments" value="{{ old('comments') }}" ></p>
        <p><label>Дата</label>
            <select name="day_payment" ><option value="">---</option>
                @foreach ($days as $day)
                <option value="{{ $day }}">{{ $day }}</option>
                @endforeach
            </select>
            <select name="month_payment" ><option value="">---</option
                    @foreach ($months as $month)
                <option value="{{ $month }}">{{ $month }}</option>
                @endforeach
            </select>
            <select name="year_payment" ><option value="">---</option>
                @foreach ($years as $year)
                <option value="{{ $year }}">{{ $year }}</option>
                @endforeach
            </select>
        </p>
        <p><label>Сумма в USD</label><input type="" name="total_price" value="{{ old('total_price') }}" ></p>

        <input type="submit" value="Добавить">

    </form>
@endsection