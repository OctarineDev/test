<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagedevsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stagedevs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_project');
            $table->integer('id_stage');
            $table->string('work_type');
            $table->integer('id_dev');
            $table->string('name_dev');
            $table->string('hour');
            $table->string('hour_price');
            $table->string('total_price');
            $table->string('day_start');
            $table->string('month_start');
            $table->string('year_start');
            $table->string('day_finish');
            $table->string('month_finish');
            $table->string('year_finish');
            $table->string('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stagedevs');
    }
}
