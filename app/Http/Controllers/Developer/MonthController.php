<?php

namespace App\Http\Controllers\Developer;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Project;
use App\Month;
use App\StageDev;
use App\Developer;
use App\Payment;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class MonthController extends Controller
{

    public function __construct(){
        $this->days = array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
        $this->months = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
        $this->years = array('2016','2017','2018','2019','2020');
    }

    public function viewForm($id){



        $developer = Developer::find($id);
        $months =  $this->months;
        $years =  $this->years;

        return view('developer.add_month')->with(compact(['developer'],['months'],['years']));

    }

    public function viewMonth($id,$id_month){

        $developer = Developer::find($id);
        $payments = Payment::
            where('id_developer', $id)
            ->where('id_month', $id_month)
            ->get();
        $month = Month::find($id_month);

        return view('developer.view_month_payment')->with(compact(['developer'],['payments'],['month']));

    }

    public function updateForm($id,$id_month){

        $month_current = Month::find($id_month);
        $developer = Developer::find($id);
        $payments = Payment::
                      where('id_developer', $id)
                      ->where('id_month', $id_month)
                      ->get();
        $months =  $this->months;
        $years =  $this->years;
        return view('developer.update_month')->with(compact(['developer'],['payments'],['month_current'],['months'],['years']));
    }

    public function addMonth(){



        $rules = [
            'month' => 'required|max:255',
            'year' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $months = new Month();

        $month = Input::get('month');
        $year = Input::get('year');
        $id = Input::get('id');


        $months->month = $month;
        $months->year = $year;

        $months->save();

        return redirect('/Developer_'.$id);

    }
    public function updateMonth(){

        $rules = [
            'month' => 'required|max:255',
            'year' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $id_month = Input::get('id');

        $months = Month::where('id', $id_month)->first();

        $month = Input::get('month');
        $year = Input::get('year');
        $id = Input::get('id_developer');

        $months->month = $month;
        $months->year = $year;

        $months->save();

        return redirect('/Developer_'.$id.'/month_'.$id_month);



    }


}
