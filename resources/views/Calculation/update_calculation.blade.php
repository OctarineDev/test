
@extends('layouts.header')

@section('content')
<p>Информация о всём проекте</p>
@if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<table>

    <tr>
        <td>Навзание проекта</td>
        <td>Заказчик</td>
        <td>КОмпания</td>
        <td>Телефон</td>
        <td>Скайп</td>
        <td>Почта</td>
    </tr>
    <a href="/update_project_{{$project->id}}">Редактировать</a>
    <tr>
        <td>{{ $project->project_name }}</td>
        <td>{{ $project->client_name }}</td>
        <td>{{ $project->company_name }}</td>
        <td>{{ $project->phone }}</td>
        <td>{{ $project->skype }}</td>
        <td>{{ $project->mail }}</td>
    </tr>

</table>
<p>Добавление нового под-проекта </p>
<form action="{{ route('update.calculation') }}" method="POST">
    {{ csrf_field() }}
    <p><input name="comments" type="" value="{{ $calculation->comments }}"></p>

    <input name="id" type="hidden" value="{{ $calculation->id }}">
    <input name="id_project" type="hidden" value="{{ $calculation->id_project }}">
    <p><label>Дата</label>
        <select name="day_payment" ><option value="">---</option>
            @foreach ($days as $day)
            @if($day == $calculation->day_payment)
            <option selected value="{{ $day }}">{{ $day }}</option>
            @else
            <option  value="{{ $day }}">{{ $day }}</option>
            @endif
            @endforeach
        </select>
        <select name="month_payment" ><option value="">---</option
                @foreach ($months as $month)
                @if($month == $calculation->month_payment)
            <option selected value="{{ $month }}">{{ $month }}</option>
            @else
            <option value="{{ $month }}">{{ $month }}</option>
            @endif
            @endforeach
        </select>
        <select name="year_payment" ><option value="">---</option>
            @foreach ($years as $year)
            @if($year == $calculation->year_payment)
            <option selected value="{{ $year }}">{{ $year }}</option>
            @else
            <option value="{{ $year }}">{{ $year }}</option>
            @endif
            @endforeach
        </select>
    </p>
    <p><label>Сумма в USD</label><input name="total_price" value="{{ $calculation->total_price }}"></p>

    <input type="submit" value="Обновить">

</form>

@endsection