@extends('layouts.header')

@section('content')
<p>Информация о сотруднике</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif



<a href="/update_developer_{{$developers->id}}">Редактировать</a>

<p><label>Ф.И.О.</label> {{ $developers->name }}</p>
<p><label>Должность</label> {{ $developers->work_type }}</p>
<p><label>Формат работы</label> {{ $developers->type_salary }}</p>
<p><label>З\п</label> {{ $developers->salary }}</p>
<p><label>Номер телефона</label> {{ $developers->phone }}</p>
<p><label>skype</label> {{ $developers->skype }}</p>
<p><label>E-mail</label> {{ $developers->mail }}</p>
<p><label>Адресс</label> {{ $developers->adress }}</p>
<p><label>Номер карты</label> {{ $developers->card }}</p>

<table>
    <tr>
        <td>Долги по проектам</td>

    </tr>
    <tr>
        <td>Проект</td>
        <td>Всего часов</td>
        <td>Оплачено часов</td>
        <td>Долг в часах</td>
        <td>Долг в $</td>
    </tr>
@foreach ($projects as $project)
    @if($project->total_hour != 0)
    <tr onclick="window.location.href='/Project_{{$project->id}}';">
        <td>{{ $project->project_name }}</td>
        <td>{{ $project->total_hour }}</td>
        <td>{{ $project->project_hour }}</td>
        <td>{{ $project->total_hour-$project->project_hour }}</td>
        <td>{{ ($project->total_hour-$project->project_hour)*$developers->salary }}</td>
    </tr>
    @endif
@endforeach
</table>
<table>
    <tr>
        <td>Отчет по месяцам</td>
        <td><a href="/Developer_{{ $id }}/addMonth">Добавить месяц</a></td>

    </tr>
    <tr>
        <td>Месяц</td>
        <td>Оплачено</td>

    </tr>
    @foreach ($months as $month)
    <tr onclick="window.location.href='/Developer_{{$id}}/month_{{$month->id}}';">
        <td>{{ $month->month }}{{ $month->year }}</td>
        <td>{{ $month->month_payment }} USD</td>
    </tr>
    @endforeach
</table>

@endsection