<?php

namespace App\Http\Controllers\Developer;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Project;
use App\Month;
use App\StageDev;
use App\Developer;
use App\Payment;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class DeveloperController extends Controller
{
    
    public function index(){

        $developers = Developer::all();
        $projects = Project::all();
        $months = Month::all();
    foreach($developers as $developer){
        foreach($months as $month){
            $payments = Payment::
                where('id_month',$month->id)
                ->where('id_developer',$developer->id)
                ->get();
            $month_payment = 0;
            foreach($payments as $payment){
                $month_payment += $payment->payment_project;
            }
            $month['month_payment'] = $month_payment;

        }
     $projects_debt = 0;
        foreach($projects as $project){
            $stagedevs = Stagedev::
                where('id_project', $project->id)
                ->where('id_dev',$developer->id)
                ->get();
            $payments = Payment::
                where('id_project', $project->id)
                ->where('id_developer',$developer->id)
                ->get();
            $project_hour = 0;
            foreach($payments as $payment){
                $project_hour += $payment->payment_hour;
            }
            $total_hour = 0;
            foreach($stagedevs as $stagedev){
                $total_hour += $stagedev->hour;
            }
            $projects_debt += ($total_hour - $project_hour)*$developer->salary;
        }

     $developer['total_debt'] = $projects_debt;
 }
        return view('developer.all_developers')->with(compact(['developers']));

    }

    public function viewForm(){

        return view('developer.add_developer');

    }

    public function viewDeveloper($id){

        $developers = Developer::find($id);
        $projects = Project::all();
        $months = Month::all();

        foreach($months as $month){
            $payments = Payment::
                        where('id_month',$month->id)
                        ->where('id_developer',$id)
                        ->get();
            $month_payment = 0;
            foreach($payments as $payment){
                $month_payment += $payment->payment_project;
            }
            $month['month_payment'] = $month_payment;

        }
        foreach($projects as $project){
            $stagedevs = Stagedev::
                            where('id_project', $project->id)
                            ->where('id_dev',$id)
                            ->get();
            $payments = Payment::
                            where('id_project', $project->id)
                            ->where('id_developer',$id)
                            ->get();
            $project_hour = 0;
            foreach($payments as $payment){
                $project_hour += $payment->payment_hour;
            }
            $total_hour = 0;
             foreach($stagedevs as $stagedev){
               $total_hour += $stagedev->hour;
             }
            $project['total_hour']=$total_hour;
            $project['project_hour']=$project_hour;
        }



        return view('developer.view_developer')->with(compact(['developers'],['projects'],['months'],['id']));

    }

    public function updateForm($id){

        $developers = Developer::find($id);
        return view('developer.update_developer')->with(compact(['developers']));
    }


    public function addDeveloper(){

        Input::flash();

        $rules = [
            'name' => 'required|unique:developers|max:255',
            'work_type' => 'required',
            'type_salary' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $developer = new Developer();

        $name = Input::get('name');
        $work_type = Input::get('work_type');
        $type_salary = Input::get('type_salary');
        $salary = Input::get('salary');
        $phone = Input::get('phone');
        $skype = Input::get('skype');
        $mail = Input::get('mail');
        $adress = Input::get('adress');
        $card = Input::get('card');

        $developer->name = $name;
        $developer->work_type = $work_type;
        $developer->type_salary = $type_salary;
        $developer->salary = $salary;
        $developer->phone = $phone;
        $developer->skype = $skype;
        $developer->mail = $mail;
        $developer->adress = $adress;
        $developer->card = $card;

        $developer->save();


        $developers = Developer::all();
        return view('developer.all_developers')->with(compact(['developers']));

    }
    public function updateDeveloper(){

        $rules = [
            'name' => 'required|max:255',
            'work_type' => 'required',
            'type_salary' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }

        $id=Input::get('id');

        $developer = Developer::where('id', $id)->first();

        $name = Input::get('name');
        $work_type = Input::get('work_type');
        $type_salary = Input::get('type_salary');
        $salary = Input::get('salary');
        $phone = Input::get('phone');
        $skype = Input::get('skype');
        $mail = Input::get('mail');
        $adress = Input::get('adress');
        $card = Input::get('card');

        $developer->name = $name;
        $developer->work_type = $work_type;
        $developer->type_salary = $type_salary;
        $developer->salary = $salary;
        $developer->phone = $phone;
        $developer->skype = $skype;
        $developer->mail = $mail;
        $developer->adress = $adress;
        $developer->card = $card;

        $developer->save();


        $developers = Developer::all();
        return view('developer.all_developers')->with(compact(['developers']));

    }


}
