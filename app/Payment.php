<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";

    public function month()
    {
        return $this->belongsTo('App\Month','id_month');
    }

}
