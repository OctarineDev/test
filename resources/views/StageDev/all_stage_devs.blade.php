@extends('layouts.header')

@section('content')
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <table>

        <tr>
            <td>Навзание проекта</td>
            <td>Заказчик</td>
            <td>КОмпания</td>
            <td>Телефон</td>
            <td>Скайп</td>
            <td>Почта</td>
        </tr>
        <a href="/update_project_{{$project->id}}">Редактировать</a>
        <tr>
            <td>{{ $project->project_name }}</td>
            <td>{{ $project->client_name }}</td>
            <td>{{ $project->company_name }}</td>
            <td>{{ $project->phone }}</td>
            <td>{{ $project->skype }}</td>
            <td>{{ $project->mail }}</td>
        </tr>
    </table>

<p>{{ $stageproject->project_type }}
<a href="/Project_{{ $project->id }}/update_stageproject_{{$stageproject->id}}">Редактировать</a>
<a href="/Project_{{ $project->id }}/stageproject_{{$stageproject->id}}/addStageDev">Добавить работу</a></p>

<table>

    <tr>
        <td>Вид работы</td>
        <td>Исполнитель</td>
        <td>Кол-во час.</td>
        <td>Час \ USD</td>
        <td>Долг</td>
    </tr>
@foreach ($stagedevs as $stagedev)
    <tr onclick="window.location.href='/Project_{{$stagedev->id_project}}/stageproject_{{$stagedev->id_stage}}/update_stagedev_{{$stagedev->id}}';">
    <td>{{ $stagedev->work_type }}</td>
    <td>{{ $stagedev->name_dev }}</td>
    <td>{{ $stagedev->hour }}</td>
    <td>{{ $stagedev->hour_price }}</td>
    <td>{{ $stagedev->total_price }}</td>
    </tr>

    @endforeach

</table>
@endsection