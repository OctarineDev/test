@extends('layouts.header')

@section('content')
<p>Информация о сотруднике</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif


<a href="/update_developer_{{$developer->id}}">Редактировать</a>

<p><label>Ф.И.О.</label> {{ $developer->name }}</p>
<p><label>Должность</label> {{ $developer->work_type }}</p>
<p><label>Формат работы</label> {{ $developer->type_salary }}</p>
<p><label>З\п</label> {{ $developer->salary }}</p>
<p><label>Номер телефона</label> {{ $developer->phone }}</p>
<p><label>skype</label> {{ $developer->skype }}</p>
<p><label>E-mail</label> {{ $developer->mail }}</p>
<p><label>Адресс</label> {{ $developer->adress }}</p>
<p><label>Номер карты</label> {{ $developer->card }}</p>

<table>
    <tr>
        <td>Отчет за {{ $month->month }}{{ $month->year }}</td>
        <td><a href="/Developer_{{ $developer->id }}/update_month_{{$month->id}}">Редактировать</a></td>
        <td><a href="/Developer_{{ $developer->id }}/month_{{ $month->id }}/addMonthPayment">Добавить отчет</a></td>

    </tr>
    <tr>
        <td>Проект</td>
        <td>Комментарий</td>
        <td>Оплачено часов</td>
        <td>Оплачено</td>
    </tr>
@foreach ($payments as $payment)
    <tr onclick="window.location.href='/Developer_1/month_1/payment_{{ $payment->id }}';">
        <td>{{ $payment->name_project }}</td>
        <td>{{ $payment->comments }}</td>
        <td>{{ $payment->payment_hour }}</td>
        <td>{{ $payment->payment_project }}</td>

    </tr>
@endforeach
</table>


@endsection