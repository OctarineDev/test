<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/',  ['as' => 'user.loginForm', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::get('/logout',  ['as' => 'user.logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['middleware' => ['is.admin']], function () {

Route::get('/statistic',  ['as' => 'all.projects', 'uses' => 'Statistic\StatisticController@index']);


Route::get('/projects',  ['as' => 'all.projects', 'uses' => 'Project\ProjectController@index']);
Route::get('/newProject',  [ 'uses' => 'Project\ProjectController@viewForm']);
Route::post('/addProject',  ['as' => 'add.project', 'uses' => 'Project\ProjectController@addProject']);
Route::get('/Project_{id}',  [ 'uses' => 'Project\ProjectController@viewProject']);
Route::get('/update_project_{id}',  ['as' => 'update.project', 'uses' => 'Project\ProjectController@updateForm']);
Route::post('/update_project',  ['as' => 'update.project', 'uses' => 'Project\ProjectController@updateProject']);
Route::post('/deleteProject_{id}',  ['as' => 'delete.project', 'uses' => 'Project\ProjectController@deleteProject']);

Route::get('/addStageProject/project_id={id}',  ['uses' => 'StageProject\StageProjectController@viewForm']);
Route::post('/addStageProject',  ['as' => 'add.stageproject', 'uses' => 'StageProject\StageProjectController@addStageProject']);
Route::get('/Project_{id}/update_stageproject_{stage_id}',  [ 'uses' => 'StageProject\StageProjectController@updateForm']);
Route::post('/successful_update_stageproject',  ['as' => 'update.stageproject', 'uses' => 'StageProject\StageProjectController@updateStageProject']);

Route::get('/developers',  ['uses' => 'Developer\DeveloperController@index']);
Route::get('/addDeveloper',  ['uses' => 'Developer\DeveloperController@viewForm']);
Route::post('/successful_dev',  ['as' => 'add.developer','uses' => 'Developer\DeveloperController@addDeveloper']);
Route::get('/Developer_{id}',  ['as' => 'view.developer','uses' => 'Developer\DeveloperController@viewDeveloper']);
Route::get('/update_developer_{id}',  ['uses' => 'Developer\DeveloperController@updateForm']);
Route::post('/successful_update_developer',  ['as' => 'update.developer','uses' => 'Developer\DeveloperController@updateDeveloper']);

Route::get('/Project_{id}/stageproject_{id_stage}',  ['uses' => 'StageDev\StageDevController@viewStageDev']);
Route::get('/Project_{id}/stageproject_{id_stage}/addStageDev',  ['uses' => 'StageDev\StageDevController@viewForm']);
Route::post('/successful_addStageDev',  ['as' => 'add.stagedev', 'uses' => 'StageDev\StageDevController@addStageDev']);
Route::get('/Project_{id}/stageproject_{id_stage}/update_stagedev_{id_stagedev}',  ['uses' => 'StageDev\StageDevController@updateForm']);
Route::post('/successful_updateStageDev',  ['as' => 'update.stagedev','uses' => 'StageDev\StageDevController@updateStageDev']);

Route::get('/Project_{id}/addCalculation',  ['uses' => 'Calculation\CalculationController@viewForm']);
Route::post('/successful_addCalculation',  ['as' => 'add.calculation', 'uses' => 'Calculation\CalculationController@addCalculation']);
Route::get('/Project_{id}/calculation_{calc_id}',  ['uses' => 'Calculation\CalculationController@viewCalculation']);
Route::get('/Project_{id}/update_calculation_{calc_id}',  [ 'uses' => 'Calculation\CalculationController@updateForm']);
Route::post('/successful_update_calculation',  [ 'as' => 'update.calculation','uses' => 'Calculation\CalculationController@updateCalculation']);

Route::get('/Developer_{id}/addMonth',  ['uses' => 'Developer\MonthController@viewForm']);
Route::post('/successful_addMonth',  ['as' => 'add.month', 'uses' => 'Developer\MonthController@addMonth']);
Route::get('/Developer_{id}/month_{id_month}',  [ 'uses' => 'Developer\MonthController@viewMonth']);
Route::get('/Developer_{id}/update_month_{id_month}',  [ 'uses' => 'Developer\MonthController@updateForm']);
Route::post('/successful_updateMonth',  ['as' => 'update.month', 'uses' => 'Developer\MonthController@updateMonth']);

Route::get('/Developer_{id}/month_{id_month}/addMonthPayment',  [ 'uses' => 'Developer\PaymentController@viewForm']);
Route::post('/successful_addMonthPayment',  [ 'as' => 'add.month.payment', 'uses' => 'Developer\PaymentController@addMonthPayment']);
Route::get('/Developer_{id}/month_{id_month}/payment_{id_payment}',  [ 'uses' => 'Developer\PaymentController@viewPayment']);
Route::get('/Developer_{id_developer}/month_{id_month}/update_payment_{id_payment}',  [ 'uses' => 'Developer\PaymentController@updateForm']);
Route::post('/successful_update_payment',  [ 'as' => 'update.payment', 'uses' => 'Developer\PaymentController@updateMonthPayment']);





});

Route::auth();