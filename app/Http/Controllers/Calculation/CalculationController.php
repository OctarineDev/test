<?php

namespace App\Http\Controllers\Calculation;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Project;
use App\StageProject;
use App\Developer;
use App\Calculation;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class CalculationController extends Controller
{

    public function __construct(){
        $this->days = array('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
        $this->months = array('Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь');
        $this->years = array('2016','2017','2018','2019','2020');
    }

    public function viewForm($id){

        $project = Project::find($id);
        $days =  $this->days;
        $months =  $this->months;
        $years =  $this->years;


        return view('calculation.add_calculation')->with(compact(['project'],['days'],['months'],['years']));

    }

    public function updateForm($id,$calc_id){

        $project = Project::find($id);
        $calculation = Calculation::
            where('id', $calc_id)
            ->where('id_project', $id)
            ->first();
        $days =  $this->days;
        $months =  $this->months;
        $years =  $this->years;

        return view('calculation.update_calculation')->with(compact(['project'],['calculation'],['days'],['months'],['years']));
    }

    public function viewCalculation($id,$calc_id){

        $project = Project::find($id);
        $calculation = Calculation::
            where('id', $calc_id)
            ->where('id_project', $id)
            ->first();


        return view('calculation.view_calculation')->with(compact(['project'],['calculation']));
    }

    public function addCalculation(){

        Input::flash();

        $rules = [
            'comments' => 'required|max:255',
            'day_payment' => 'required|max:255',
            'month_payment' => 'required|max:255',
            'year_payment' => 'required|max:255',
            'total_price' => 'required|max:255',

        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }


        $calculation = new Calculation();

        $id_project = Input::get('id_project');
        $comments = Input::get('comments');
        $day_payment = Input::get('day_payment');
        $month_payment = Input::get('month_payment');
        $year_payment = Input::get('year_payment');
        $total_price = Input::get('total_price');


        $calculation->id_project = $id_project;
        $calculation->comments = $comments;
        $calculation->day_payment = $day_payment;
        $calculation->month_payment = $month_payment;
        $calculation->year_payment = $year_payment;
        $calculation->total_price = $total_price;

        $calculation->save();

        return redirect('/Project_'.$id_project);

    }

    public function updateCalculation(){


        $rules = [
            'comments' => 'required|max:255',
            'day_payment' => 'required|max:255',
            'month_payment' => 'required|max:255',
            'year_payment' => 'required|max:255',
            'total_price' => 'required|max:255',

        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }


        $id_project = Input::get('id_project');
        $calc_id = Input::get('id');

        $calculation = Calculation::
              where('id', $calc_id)
            ->where('id_project', $id_project)
            ->first();

        $comments = Input::get('comments');
        $day_payment = Input::get('day_payment');
        $month_payment = Input::get('month_payment');
        $year_payment = Input::get('year_payment');
        $total_price = Input::get('total_price');


        $calculation->comments = $comments;
        $calculation->day_payment = $day_payment;
        $calculation->month_payment = $month_payment;
        $calculation->year_payment = $year_payment;
        $calculation->total_price = $total_price;

        $calculation->save();

        return redirect('/Project_'.$id_project);

    }

}
