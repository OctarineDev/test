<?php

use Illuminate\Database\Seeder;
use App\Calculation as Calculation;


class CalculationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Calculation::truncate();

        Calculation::create( [
            'id_project' => '1' ,
            'comments' => 'За разработку сайта 30%' ,
            'day_payment' => '1' ,
            'month_payment' => 'Январь' ,
            'year_payment' => '2017' ,
            'total_price' => '400' ,
        ] );
    }
}
