<?php

namespace App\Http\Controllers\Statistic;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Project;
use App\StageProject;
use App\StageDev;
use App\Calculation;
use App\Payment;
use App\Month;

use \Validator;
use \Redirect;
use Illuminate\Support\Facades\Input;

class StatisticController extends Controller
{
    
    public function index(){

        $months = Month::all();
        $projects = Project::all();

     foreach($months as $month){

         $month_payments = Payment::where('id_month', $month->id)->get();
         $total_debt = 0;
         foreach($month_payments as $month_payment){

             $total_debt+=$month_payment->payment_project;

         }
         $month['total_debt'] = $total_debt;

         $stage_payments = Calculation::where('month_payment', $month->month)
                                        ->where('year_payment',$month->year)
                                        ->get();
         $total_salary = 0;
         foreach($stage_payments as $stage_payment){

             $total_salary+=$stage_payment->total_price;

         }
         $month['total_salary'] = $total_salary;


     }
        $debt = 0;
        foreach($projects as $project){

            $payments = Payment::where('id_project', $project->id)->get();
            $total_debt = 0;
            foreach($payments as $payment){

                $total_debt+=$payment->payment_project;

            }
            $project['total_debt'] = $total_debt;

            $stage_devs = StageDev::where('id_project', $project->id)->get();
            $summary_debt = 0;
            foreach($stage_devs as $stage_dev){

                $summary_debt+=$stage_dev->total_price;

            }

            $calculations = Calculation::where('id_project', $project->id)->get();
            $total_salary = 0;
            foreach($calculations as $calculation){

                $total_salary+=$calculation->total_price;

            }
         $project['total_salary'] = $total_salary;
         $salary = $total_salary-$total_debt;
         $project['salary']= $salary;
          $project['debt']= abs($summary_debt - $total_salary);
          $debt+=abs($project['debt']);




     }


    return view('statistic.view_statistic')->with(compact(['months'],['projects'],['debt']));

    }


}
