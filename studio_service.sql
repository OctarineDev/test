-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 14 2017 г., 12:52
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `studio_service`
--

-- --------------------------------------------------------

--
-- Структура таблицы `calculations`
--

CREATE TABLE IF NOT EXISTS `calculations` (
  `id` int(10) unsigned NOT NULL,
  `id_project` int(11) NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_payment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `calculations`
--

INSERT INTO `calculations` (`id`, `id_project`, `comments`, `day_payment`, `month_payment`, `year_payment`, `total_price`, `created_at`, `updated_at`) VALUES
(1, 1, 'За брендбук', '20', 'Февраль', '2017', '1000', '2017-04-07 12:29:06', '2017-04-07 12:29:06'),
(2, 1, 'предоплата за сайт', '20', 'Февраль', '2017', '900', '2017-04-07 12:29:28', '2017-04-07 12:29:38'),
(3, 2, 'предоплата за сайт', '13', 'Март', '2017', '300', '2017-04-07 12:48:19', '2017-04-07 12:48:19'),
(4, 2, 'остаток за сайт', '29', 'Март', '2017', '300', '2017-04-07 12:48:33', '2017-04-07 12:48:33'),
(5, 2, 'за лого', '6', 'Апрель', '2017', '250', '2017-04-07 12:48:52', '2017-04-07 12:48:52');

-- --------------------------------------------------------

--
-- Структура таблицы `developers`
--

CREATE TABLE IF NOT EXISTS `developers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `work_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_salary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `developers`
--

INSERT INTO `developers` (`id`, `name`, `work_type`, `type_salary`, `salary`, `phone`, `adress`, `card`, `skype`, `mail`, `created_at`, `updated_at`) VALUES
(1, 'Анатолий Сахно', 'backend', '2', '5', '+380939614254', '', '', '', '', '2017-04-07 12:13:51', '2017-04-07 12:13:51'),
(2, 'Михаил Чепурняк', 'Дизайнер', '2', '20', '+380939614254', '', '', '', '', '2017-04-07 12:14:20', '2017-04-07 12:14:20'),
(3, 'Евгений Билык', 'Sales', '1', '80', '+380939614254', '', '', '', '', '2017-04-07 12:15:09', '2017-04-07 12:15:09'),
(4, 'Максим Гончаров', 'frontend', '2', '2', '+380939614254', '', '', '', '', '2017-04-07 12:15:44', '2017-04-07 12:15:44');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_02_171458_create_projects_table', 1),
(4, '2017_03_02_171537_create_developers_table', 1),
(5, '2017_03_04_102455_create_stagedevs_table', 1),
(6, '2017_03_06_144343_create_stageprojects_table', 1),
(7, '2017_03_09_134448_create_calculations_table', 1),
(8, '2017_03_13_135228_create_months_table', 1),
(9, '2017_03_13_141111_create_payments_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `months`
--

CREATE TABLE IF NOT EXISTS `months` (
  `id` int(10) unsigned NOT NULL,
  `month` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `months`
--

INSERT INTO `months` (`id`, `month`, `year`, `created_at`, `updated_at`) VALUES
(1, 'Январь', '2017', '2017-04-07 12:36:27', '2017-04-07 12:36:27'),
(2, 'Февраль', '2017', '2017-04-07 12:36:35', '2017-04-07 12:36:35'),
(3, 'Март', '2017', '2017-04-07 12:36:49', '2017-04-07 12:36:49'),
(4, 'Апрель', '2017', '2017-04-07 12:36:55', '2017-04-07 12:36:55');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) unsigned NOT NULL,
  `id_month` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_project` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_developer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_project` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_hour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_project` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `payments`
--

INSERT INTO `payments` (`id`, `id_month`, `id_project`, `id_developer`, `name_project`, `comments`, `payment_hour`, `payment_project`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '2', 'Moltobrand', 'За брендбук', '30', '600', '2017-04-07 12:38:20', '2017-04-07 12:38:20'),
(2, '3', '1', '2', 'Moltobrand', 'за лого анимацию', '3', '60', '2017-04-07 12:39:03', '2017-04-07 12:39:03'),
(3, '3', '2', '1', 'harbros', 'за харброс', '14', '75', '2017-04-07 12:50:13', '2017-04-07 12:50:13'),
(4, '3', '2', '4', 'harbros', 'за харброс', '80', '160', '2017-04-07 12:53:16', '2017-04-07 12:53:16'),
(5, '4', '1', '4', 'Moltobrand', 'часть за работу', '88', '176', '2017-04-07 12:54:19', '2017-04-07 12:54:19'),
(6, '4', '2', '3', 'Moltobrand', 'Процент за сайт', '0', '60', '2017-04-07 12:55:54', '2017-04-07 12:55:54'),
(7, '4', '2', '3', 'Moltobrand', 'Процент за сайт', '0', '25', '2017-04-07 12:56:07', '2017-04-07 12:56:07');

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL,
  `project_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `skype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `client_name`, `company_name`, `status`, `phone`, `skype`, `mail`, `created_at`, `updated_at`) VALUES
(1, 'Moltobrand', 'Роман Кармазин', 'Moltobrand', 'Открыт', '+38096574554', '', '', '2017-04-07 12:11:58', '2017-04-07 12:11:58'),
(2, 'harbros', 'Роман Горгун', 'Харброс', 'Открыт', '+3809396142541', '', '', '2017-04-07 12:39:30', '2017-04-07 12:39:30'),
(74, 'mmmmmmmmmmmmmm', 'mmmmmmmmmmmmmmmmmmmm', '', 'Открыт', 'mmmmmm', '', '', '2017-04-11 13:07:28', '2017-04-11 13:07:28');

-- --------------------------------------------------------

--
-- Структура таблицы `stagedevs`
--

CREATE TABLE IF NOT EXISTS `stagedevs` (
  `id` int(10) unsigned NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_stage` int(11) NOT NULL,
  `work_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_dev` int(11) NOT NULL,
  `name_dev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hour_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_start` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day_finish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month_finish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year_finish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `stagedevs`
--

INSERT INTO `stagedevs` (`id`, `id_project`, `id_stage`, `work_type`, `id_dev`, `name_dev`, `hour`, `hour_price`, `total_price`, `day_start`, `month_start`, `year_start`, `day_finish`, `month_finish`, `year_finish`, `comments`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'дизайн', 2, 'Михаил Чепурняк', '30', '20', '1000', '2', 'Январь', '2017', '10', 'Февраль', '2017', 'Миша ублюдок, долго делал', '2017-04-07 12:19:54', '2017-04-07 12:32:54'),
(2, 1, 2, 'Дизайн Сайта', 2, 'Михаил Чепурняк', '40', '20', '800', '11', 'Февраль', '2017', '31', 'Март', '2017', 'Миша, мудила, долго делал. И 100 раз переделывал. Припомнить ему это.', '2017-04-07 12:22:43', '2017-04-07 12:22:43'),
(3, 1, 2, 'верстка', 4, 'Максим Гончаров', '144', '5', '400', '22', 'Март', '2017', '20', 'Апрель', '2017', '', '2017-04-07 12:24:15', '2017-04-07 12:46:09'),
(4, 1, 2, 'backend', 1, 'Анатолий Сахно', '55', '10', '550', '22', 'Март', '2017', '20', 'Апрель', '2017', '', '2017-04-07 12:25:01', '2017-04-07 12:28:31'),
(5, 1, 2, 'Лого анимация', 2, 'Михаил Чепурняк', '3', '20', '60', '25', 'Март', '2017', '27', 'Март', '2017', '', '2017-04-07 12:27:06', '2017-04-07 12:35:27'),
(6, 2, 3, 'Дизайн', 2, 'Михаил Чепурняк', '10', '20', '200', '10', 'Март', '2017', '11', 'Март', '2017', 'Мудила берет много денег', '2017-04-07 12:40:52', '2017-04-07 12:40:52'),
(7, 2, 3, 'верстка', 4, 'Максим Гончаров', '80', '2', '160', '14', 'Март', '2017', '28', 'Март', '2017', '', '2017-04-07 12:43:04', '2017-04-07 12:43:04'),
(8, 2, 3, 'backend', 1, 'Анатолий Сахно', '14', '10', '240', '14', 'Март', '2017', '26', 'Март', '2017', '', '2017-04-07 12:45:05', '2017-04-07 12:45:05'),
(9, 2, 4, 'Дизайн логотипа', 2, 'Михаил Чепурняк', '8', '20', '250', '30', 'Март', '2017', '4', 'Апрель', '2017', '', '2017-04-07 12:47:52', '2017-04-07 12:47:52');

-- --------------------------------------------------------

--
-- Структура таблицы `stageprojects`
--

CREATE TABLE IF NOT EXISTS `stageprojects` (
  `id` int(10) unsigned NOT NULL,
  `id_project` int(11) NOT NULL,
  `project_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_dev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_dev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `project_percent` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `stageprojects`
--

INSERT INTO `stageprojects` (`id`, `id_project`, `project_type`, `id_dev`, `name_dev`, `project_percent`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Разработка брендбука', '3', 'Евгений Билык', '100', 'Открыт', '2017-04-07 12:16:40', '2017-04-07 12:21:07'),
(2, 1, 'Разработка Сайта', '3', 'Евгений Билык', '180', 'Открыт', '2017-04-07 12:20:32', '2017-04-07 12:21:22'),
(3, 2, 'Разработка сайта', '3', 'Евгений Билык', '60', 'Закрыт', '2017-04-07 12:39:46', '2017-04-07 12:55:54'),
(4, 2, 'Разработка логотипа', '3', 'Евгений Билык', '25', 'Закрыт', '2017-04-07 12:46:54', '2017-04-07 12:56:07');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Радик Падик', 'rozhden.t@gmail.com', '$2y$10$fniBNOpRhuRY5FkfFqF9jubxPTnGvKM3k70VlkzLBibfHwIOmmwYm', NULL, '2017-04-07 12:09:01', '2017-04-07 12:09:01');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `calculations`
--
ALTER TABLE `calculations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `developers`
--
ALTER TABLE `developers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `months`
--
ALTER TABLE `months`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stagedevs`
--
ALTER TABLE `stagedevs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `stageprojects`
--
ALTER TABLE `stageprojects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `calculations`
--
ALTER TABLE `calculations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `developers`
--
ALTER TABLE `developers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `months`
--
ALTER TABLE `months`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT для таблицы `stagedevs`
--
ALTER TABLE `stagedevs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `stageprojects`
--
ALTER TABLE `stageprojects`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
