<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    public function payments()
    {
        return $this->hasMany('App\Payment','id_month');
    }
}
