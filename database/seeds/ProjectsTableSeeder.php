<?php

use Illuminate\Database\Seeder;
use App\Project as Project;


class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::truncate();

        DB::table('projects')->insert([
            'project_name' => 'Agro7Trade',
            'client_name' => 'Капустян Владимир',
            'company_name' => 'Agro7Trade',
            'status' => 'Открыт',
            'phone' => '+38 (098) 747-75-18',
            'skype' => '-',
            'mail' => 'agro7trade@mail.ru',

        ]);
        DB::table('projects')->insert([
            'project_name' => 'Blugerman',
            'client_name' => 'Рома Кармазин',
            'company_name' => 'Bluger',
            'status' => 'Открыт',
            'phone' => '+38 (066) 666-66-66',
            'skype' => 'bluger.molto',
            'mail' => 'bluger.company@mail.ru',

        ]);
    }




}
