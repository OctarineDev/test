@extends('layouts.header')

@section('content')
    <p>Информация о всём проекте</p>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <table>

        <tr>
            <td>Навзание проекта</td>
            <td>Заказчик</td>
            <td>КОмпания</td>
            <td>Телефон</td>
            <td>Скайп</td>
            <td>Почта</td>
        </tr>
        <a href="/update_project_{{$project->id}}">Редактировать</a>
        <tr>
            <td>{{ $project->project_name }}</td>
            <td>{{ $project->client_name }}</td>
            <td>{{ $project->company_name }}</td>
            <td>{{ $project->phone }}</td>
            <td>{{ $project->skype }}</td>
            <td>{{ $project->mail }}</td>
        </tr>

    </table>
    <p>Добавление нового под-проекта </p>
    <form action="{{ route('add.stageproject') }}" method="POST">
        {{ csrf_field() }}
        <input name="id_project" value="{{ $project->id }}" type="hidden">
        <p><label>Наименование</label><input type="" name="project_type" value="{{ old('project_type') }}" >
            <label>Статус</label><select name="status">
                <option value="Открыт">Открыт</option>
                <option value="Закрыт">Закрыт</option>
            </select>
         </p>
        <p><label>Кто привел заказчика?</label>
            <select name="id_dev">
                @foreach ($developers as $developer)
                <option value="{{ $developer->id }}">{{ $developer->name }}</option>
                @endforeach
            </select>
        </p>
        <p>
            <label>Сумма за проект</label><input type="" name="project_percent" value="{{ old('project_percent') }}" >
        </p>
        <input type="submit" value="Добавить">

    </form>
@endsection