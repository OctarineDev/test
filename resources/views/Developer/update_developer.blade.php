@extends('layouts.header')

@section('content')
        <p>Информация о сотруднике</p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ route('update.developer')}}" method="POST">
            {{ csrf_field() }}

            <input type="hidden" name="id" value="{{ $developers->id }}">
            <p><label>Ф.И.О.</label><input type="" name="name" value="{{ $developers->name }}" ></p>
            <p><label>Должность</label><input type="" name="work_type" value="{{ $developers->work_type }}" ></p>
            <p><label>Формат работы</label><select size="1" name="type_salary">
                    @if ($developers->type_salary == 1)
                    <option selected value="1">По ЗП</option>
                    <option value="2">По проектно</option>
                    @else
                    <option value="1">По ЗП</option>
                    <option selected value="2">По проектно</option>
                    @endif
                </select></p>
            <p><label>З\п</label><input type="" name="salary" value="{{ $developers->salary }}"></p>
            <p><label>Номер телефона</label><input type="" name="phone" value="{{ $developers->phone }}" ></p>
            <p><label>skype</label><input type="" name="skype" value="{{ $developers->skype }}" ></p>
            <p><label>E-mail</label><input type="" name="mail" value="{{ $developers->mail }}" ></p>
            <p><label>Адресс</label><input type="" name="adress" value="{{ $developers->adress }}" ></p>
            <p><label>Номер карты</label><input type="" name="card" value="{{ $developers->card }}" ></p>

            <input type="submit" value="Обновить">

        </form>
@endsection