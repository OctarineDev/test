@extends('layouts.header')

@section('content')
        <p>Информация о сотруднике</p>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    <a href="/update_developer_{{$developer->id}}">Редактировать</a>

    <p><label>Ф.И.О.</label> {{ $developer->name }}</p>
    <p><label>Должность</label> {{ $developer->work_type }}</p>
    <p><label>Формат работы</label> {{ $developer->type_salary }}</p>
    <p><label>З\п</label> {{ $developer->salary }}</p>
    <p><label>Номер телефона</label> {{ $developer->phone }}</p>
    <p><label>skype</label> {{ $developer->skype }}</p>
    <p><label>E-mail</label> {{ $developer->mail }}</p>
    <p><label>Адресс</label> {{ $developer->adress }}</p>
    <p><label>Номер карты</label> {{ $developer->card }}</p>


    <h1>{{ $payment->name_project }}</h1>
        <form action="{{ route('update.payment') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $payment->id }}" >
            <input type="hidden" name="id_developer" value="{{ $payment->id_developer }}" >
            <input type="hidden" name="id_month" value="{{ $payment->id_month }}" >
            <p><label>Проект</label>
                <select name="name_project">
                    @foreach ($projects as $project)
                    @if($payment->name_project == $project->project_name)
                    <option selected value="{{ $project->project_name }}">{{ $project->project_name }}</option>
                    @else
                    <option value="{{ $project->project_name }}">{{ $project->project_name }}</option>
                    @endif
                    @endforeach
                </select>
            </p>
            <p><label>Оплачено часов</label><input type="" value="{{ $payment->payment_hour }}" name="payment_hour" ></p>
            <p><label>Оплачено (USD)</label><input type="" value="{{ $payment->payment_project }}" name="payment_project" ></p>
            <p><label>Комментарий</label><input type="" value="{{ $payment->comments }}" name="comments" ></p>
            <input type="submit" value="Обновить">
        </form>
@endsection